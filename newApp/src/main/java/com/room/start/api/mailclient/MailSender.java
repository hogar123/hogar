package com.room.start.api.mailclient;

import java.io.IOException;

import com.sendgrid.Content;
import com.sendgrid.Email;
import com.sendgrid.Mail;
import com.sendgrid.Method;
import com.sendgrid.Request;
import com.sendgrid.Response;
import com.sendgrid.SendGrid;

public class MailSender {

	public static void sendMail(MailContent mailContent) {

		try {
			Email from = new Email(mailContent.getMailFrom());
			String subject = mailContent.getMailSubject();
			Email to = new Email(mailContent.getMailTo());
			Content content = new Content("text/plain", mailContent.getMailContent());
			Mail mail = new Mail(from, subject, to, content);

			SendGrid sg = new SendGrid("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");
			Request request = new Request();

			request.method = Method.POST;
			request.endpoint = "mail/send";
			request.body = mail.build();
			Response response = sg.api(request);

			System.out.println("Mail response return code ::" + response.statusCode);
			System.out.println("Mail response body ::" + response.body);
			System.out.println("Mail response headers :: " + response.headers);

		} catch (IOException ex) {

			ex.printStackTrace();
		}
	}
}