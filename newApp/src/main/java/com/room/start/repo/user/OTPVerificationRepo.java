package com.room.start.repo.user;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.room.start.model.user.OTPVerificationModel;

public interface OTPVerificationRepo extends JpaRepository<OTPVerificationModel, Integer>{
	
	@Query("select * from otp_table where mobile = :mobile")
	OTPVerificationModel findByMobile(@Param("mobile") String mobile);

}
