package com.room.start.repo.user;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.room.start.model.user.UserDetails;

public interface UserDetailsRepo extends JpaRepository<UserDetails, Integer>{

	@Query(value = "SELECT * FROM users u WHERE u.mobile = ?1 or email = ?2", nativeQuery=true)
	UserDetails validateUser(String mobileNum, String email);
}
