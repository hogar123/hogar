package com.room.start.repo.user;

import org.springframework.data.jpa.repository.JpaRepository;

import com.room.start.model.user.MailVerificationModel;

public interface MailVerificationRepo extends JpaRepository<MailVerificationModel, Integer>{

}
