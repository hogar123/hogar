package com.room.start.model.user;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity()
@Table(name = "otp_table")
public class OTPVerificationModel {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	int id;

	@Column(name = "mobile")
	String mobile;
	
	@Column(name = "latest_otp")
	String latestOtp;
	
	@Column(name = "rcount")
	int rCount;
	
	@Column(name = "delivered")
	int delivered;
	
	@Column(name = "verified")
	int verified;
	
	@Column(name = "remarks")
	String remarks;
	
	@Column(name = "sent_at")
	String sentAt;
	
	@Column(name="verified_at")
	String verifiedAt;

	public OTPVerificationModel(){
		super();
	}
	
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getMobile() {
		return mobile;
	}

	public void setMobile(String mobile) {
		this.mobile = mobile;
	}

	public String getLatestOtp() {
		return latestOtp;
	}

	public void setLatestOtp(String latestOtp) {
		this.latestOtp = latestOtp;
	}

	public int getrCount() {
		return rCount;
	}

	public void setrCount(int rCount) {
		this.rCount = rCount;
	}

	public int getDelivered() {
		return delivered;
	}

	public void setDelivered(int delivered) {
		this.delivered = delivered;
	}

	public int getVerified() {
		return verified;
	}

	public void setVerified(int verified) {
		this.verified = verified;
	}

	public String getRemarks() {
		return remarks;
	}

	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}

	public String getSentAt() {
		return sentAt;
	}

	public void setSentAt(String sentAt) {
		this.sentAt = sentAt;
	}

	public String getVerifiedAt() {
		return verifiedAt;
	}

	public void setVerifiedAt(String verifiedAt) {
		this.verifiedAt = verifiedAt;
	}
}
