package com.room.start.constants;

public class AppConstants {
	
	// Common JSON data formats
	public static final String REQUEST_IDENTIFIER = "id";
	public static final String REQUEST_CONTENT = "content";
	
	public static final String RESPONSE_CONTENT = "resp";
	
	public static final String UNIQUE_ID = "uniqueId";
	public static final String CODE = "code";
	public static final String MESSAGE = "message";
	public static final String DEVICE_ID = "deviceId";
	
	public static final String DB_FORMAT = "yyyy-MM-dd HH:mm:ss";	
	// User details
	public static final String MOBILE_NUMBER = "mobile";
	public static final String EMAIL_ADDRESS = "mail";
	
	public static final String USER_NAME = "username";
	public static final String PASSWORD = "password";
	public static final String CITY_ID = "cityID";
	
	public static final String VERIFICATION_CODE = "verCode";
	
	
	
	public static final String OTP_VERIFIED = "OTP Verified";
	
}
