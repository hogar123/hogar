package com.room.start.constants;

public class AppErrorCode {

	public static final String SUCCESS = "0";
	
	public static final String INVALID_REQUEST_PARAMETERS = "HOG001";
	public static final String REQUEST_FAILED = "HOG002";
	
	public static final String USER_ALREADY_EXISTS = "HOGU001";
	
	
	public static final String INVALID_VERIFICATION_CODE = "HOGV001";
	public static final String OTP_ALREADY_VERIFIED = "HOGV002";
}
