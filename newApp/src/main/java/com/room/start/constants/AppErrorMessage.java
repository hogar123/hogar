package com.room.start.constants;

public class AppErrorMessage {
	
	public static final String SUCCESS = "SUCCESS";
	
	public static final String INVALID_REQUEST_PARAMETERS = "Invalid request parameters";
	public static final String REQUEST_FAILED = "We are unable to process the request now";

	public static final String USER_ALREADY_EXISTS = "User mobile / mail already exists";
	
	public static final String INVALID_VERIFICATION_CODE = "Invalid verification code";
	public static final String OTP_ALREADY_VERIFIED = "Verification code already validated";
}
