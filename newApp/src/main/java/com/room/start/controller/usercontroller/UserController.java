package com.room.start.controller.usercontroller;

import org.json.me.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.room.start.api.mailclient.MailContent;
import com.room.start.api.mailclient.MailSender;
import com.room.start.base.BaseController;
import com.room.start.constants.AppConstants;
import com.room.start.constants.AppErrorCode;
import com.room.start.constants.AppErrorMessage;
import com.room.start.model.user.UserDetails;
import com.room.start.repo.user.UserDetailsRepo;

@RestController
@RequestMapping("/user")
public class UserController extends BaseController {

	@Autowired
	UserDetailsRepo userRepo;

	@RequestMapping(value = "/CheckUserAvailability/1.0.0", method = RequestMethod.POST, produces = "application/json")
	@ResponseBody
	public String validateUser(@RequestBody String body) {
		try {
			validateRequest(body, "user", "validateUser", "1.0.0");

			JSONObject contentObj = hRequest.getContentObj();

			String mobNum = contentObj.getString(AppConstants.MOBILE_NUMBER);
			String mailId = contentObj.getString(AppConstants.EMAIL_ADDRESS);

			UserDetails uDetails = userRepo.validateUser(mobNum, mailId);

			if (uDetails == null) {
				hogarResp.setErrorCode(AppErrorCode.SUCCESS);
				hogarResp.setErrorMessage(AppErrorMessage.SUCCESS);
			} else {
				hogarResp.setErrorCode(AppErrorCode.USER_ALREADY_EXISTS);
				hogarResp.setErrorMessage(AppErrorMessage.USER_ALREADY_EXISTS);
			}

		} catch (Exception e) {
			try {
				frameErrorResponse(e);
			} catch (Exception e1) {
				return "{}";
			}
		}

		return hogarResp.toString();
	}

	@RequestMapping(value = "/Signup/1.0.0", method = RequestMethod.POST, produces = "application/json")
	@ResponseBody
	public String signupUser(@RequestBody String body) {

		try {
			validateRequest(body, "user", "Signup", "1.0.0");

			JSONObject contentObj = hRequest.getContentObj();

			String mobNum = contentObj.getString(AppConstants.MOBILE_NUMBER);
			String mailId = contentObj.getString(AppConstants.EMAIL_ADDRESS);
			String loginName = contentObj.getString(AppConstants.USER_NAME);
			String password = contentObj.getString(AppConstants.PASSWORD);

			UserDetails uDetails = userRepo.validateUser(mobNum, mailId);

			if (uDetails == null) {

				UserDetails newUser = new UserDetails();

				newUser.setUsername(loginName);
				newUser.setMobile(mobNum);
				newUser.setEmail(mailId);
				newUser.setPassword(password);
				newUser.setuType(1);

				MailContent mContent = new MailContent();
				mContent.setMailFrom("govind.dbe@gmail.com");
				mContent.setMailTo(mailId);
				mContent.setMailSubject("Mail verification for user :: " + loginName);
				mContent.setMailContent("Please enter this verification number :: " + (Math.random() * 100000));

				MailSender.sendMail(mContent);

				userRepo.save(newUser);

			} else {
				hogarResp.setErrorCode(AppErrorCode.USER_ALREADY_EXISTS);
				hogarResp.setErrorMessage(AppErrorMessage.USER_ALREADY_EXISTS);
			}

		} catch (Exception e) {
			e.printStackTrace();
			try {
				frameErrorResponse(e);
			} catch (Exception e1) {
				return "{}";
			}
		}

		return hogarResp.toString();
	}
}
