package com.room.start.controller.usercontroller;

import java.util.Date;

import org.json.me.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.room.start.base.BaseController;
import com.room.start.base.HogarUtilities;
import com.room.start.constants.AppConstants;
import com.room.start.constants.AppErrorCode;
import com.room.start.constants.AppErrorMessage;
import com.room.start.model.user.OTPVerificationModel;
import com.room.start.repo.user.MailVerificationRepo;
import com.room.start.repo.user.OTPVerificationRepo;

@RestController
@RequestMapping(value="/verify")
public class CodeVerificationController extends BaseController{

	@Autowired
	MailVerificationRepo mailRepo;
	
	@Autowired
	OTPVerificationRepo otpRepo;
	
	
	@RequestMapping(value="/MailVerification/1.0.0", method=RequestMethod.GET)
	@ResponseBody
	public String verifyMailCode(@RequestBody String body){
		
		
		
		return hogarResp.toString();
	}
	
	@RequestMapping(value="/OTPVerification/1.0.0", method=RequestMethod.GET)
	@ResponseBody
	public String verifyOTP(@RequestBody String body){
		
		try {
			validateRequest(body, "Verify", "OTPVerification", "1.0.0");

			JSONObject contentObj = hRequest.getContentObj();

			String mobNum = contentObj.getString(AppConstants.MOBILE_NUMBER);
			String verificationCode = contentObj.getString(AppConstants.VERIFICATION_CODE);

			OTPVerificationModel otpModel = otpRepo.findByMobile(mobNum);
			
			if(otpModel == null || (!otpModel.getLatestOtp().equalsIgnoreCase(verificationCode))){
				hogarResp.setErrorCode(AppErrorCode.INVALID_VERIFICATION_CODE);
				hogarResp.setErrorMessage(AppErrorMessage.INVALID_VERIFICATION_CODE);
			}else{
				
				if(otpModel.getVerified() == 0){
					hogarResp.setErrorCode(AppErrorCode.OTP_ALREADY_VERIFIED);
					hogarResp.setErrorMessage(AppErrorMessage.OTP_ALREADY_VERIFIED);
				}else{
					
					otpModel.setVerified(1);
					otpModel.setVerifiedAt(HogarUtilities.formatToString(new Date(), AppConstants.DB_FORMAT));
					otpModel.setRemarks(AppConstants.OTP_VERIFIED);
					
					otpRepo.save(otpModel);
					
					hogarResp.setErrorCode(AppErrorCode.SUCCESS);
					hogarResp.setErrorMessage(AppErrorMessage.SUCCESS);
				}
			}

		} catch (Exception e) {
			try {
				frameErrorResponse(e);
			} catch (Exception e1) {
				return "{}";
			}
		}
		
		return hogarResp.toString();
	}
}
