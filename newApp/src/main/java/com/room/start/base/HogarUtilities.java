package com.room.start.base;

import java.math.BigInteger;
import java.security.MessageDigest;
import java.text.SimpleDateFormat;
import java.util.Date;

public class HogarUtilities {
	
	
	public static String calculateMD5(String data){

		String md5String = "";
		
		try {
			MessageDigest md5 = MessageDigest.getInstance("MD5");
			
			md5.update(data.getBytes(), 0 , data.length());
			md5String = new BigInteger(1, md5.digest()).toString(16);

            while (md5String.length() < 32)
                md5String = "0" + md5String;
            
		} catch (Exception e) {

		}
		
		return md5String;
	}

	public static String formatToString(Date d, String format){
		
		SimpleDateFormat sdf = new SimpleDateFormat(format);
		
		return sdf.format(d);
	}
}
