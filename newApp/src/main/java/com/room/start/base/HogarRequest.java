package com.room.start.base;

import org.json.me.JSONException;
import org.json.me.JSONObject;

import com.room.start.constants.AppConstants;

public class HogarRequest extends JSONObject {

	private JSONObject contentObj = null;
	private String uniqueID = null;
	private String idObj = null;
	
	public HogarRequest(String request) throws JSONException {

		super(request);

		if (this.has(AppConstants.REQUEST_IDENTIFIER))
			idObj = this.getString(AppConstants.REQUEST_IDENTIFIER);

		contentObj = this.getJSONObject(AppConstants.REQUEST_CONTENT);
	}

	public JSONObject getContentObj() {

		return contentObj;
	}

	public String getIdObject() {

		return idObj;
	}

	public void setUniqueID(String uniqueID) {

		this.uniqueID = uniqueID;
	}

	public String getUniqueID() {

		return uniqueID;
	}

}
