package com.room.start.base;

import org.json.me.JSONException;

import com.room.start.constants.AppErrorCode;
import com.room.start.constants.AppErrorMessage;

public class BaseController {

	protected HogarResponse hogarResp = null;
	protected HogarRequest hRequest = null;
	protected String apiCode = AppErrorCode.SUCCESS;
	protected String apiMessage = AppErrorMessage.SUCCESS;

	protected String SVC_GROUP = "";
	protected String SVC_NAME = "";
	protected String SVC_VERSION = "";

	protected boolean validateRequest(String request, String serviceGroup, String serviceName, String serviceVersion)
			throws Exception {

		this.SVC_GROUP = serviceGroup;
		this.SVC_NAME = serviceName;
		this.SVC_VERSION = serviceVersion;

		String uniqueId = HogarUtilities.calculateMD5(request + System.currentTimeMillis());

		logRequest(request, serviceGroup, serviceName, serviceVersion);

		boolean isValidRequest = false;

		try {

			hRequest = new HogarRequest(request);
			hRequest.setUniqueID(uniqueId);

			hogarResp = new HogarResponse(hRequest);

			logRequest(request, serviceGroup, serviceName, serviceVersion);

			isValidRequest = true;

		} catch (JSONException e) {

			e.printStackTrace();

			hogarResp = new HogarResponse();
			hogarResp.setErrorCode(AppErrorCode.INVALID_REQUEST_PARAMETERS);
			hogarResp.setErrorMessage(AppErrorMessage.INVALID_REQUEST_PARAMETERS);
			hogarResp.setUniqueId(uniqueId);
		}

		return isValidRequest;
	}

	protected void logRequest(String request, String serviceGroup, String serviceName, String serviceVersion) {

	}

	protected void frameErrorResponse(Exception e) throws Exception {
		
		if (e instanceof JSONException) {

			hogarResp = new HogarResponse();
			hogarResp.setErrorCode(AppErrorCode.INVALID_REQUEST_PARAMETERS);
			hogarResp.setErrorMessage(AppErrorMessage.INVALID_REQUEST_PARAMETERS);
		} else {
			hogarResp = new HogarResponse();
			hogarResp.setErrorCode(AppErrorCode.REQUEST_FAILED);
			hogarResp.setErrorMessage(AppErrorMessage.REQUEST_FAILED);
		}
	}

}
