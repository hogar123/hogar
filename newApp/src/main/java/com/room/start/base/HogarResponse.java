package com.room.start.base;

import org.json.me.JSONException;
import org.json.me.JSONObject;

import com.room.start.constants.AppConstants;
import com.room.start.constants.AppErrorCode;
import com.room.start.constants.AppErrorMessage;

public class HogarResponse {

	private JSONObject hogarResponse = null;
	private JSONObject respObject = null;

	public HogarResponse() {

		hogarResponse = new JSONObject();
		respObject = new JSONObject();
	}

	public HogarResponse(HogarRequest hRequest) throws JSONException {

		hogarResponse = new JSONObject();
		respObject = new JSONObject();

		hogarResponse.put(AppConstants.REQUEST_IDENTIFIER, hRequest.getIdObject());
		hogarResponse.put(AppConstants.UNIQUE_ID, hRequest.getUniqueID());

	}

	public void addToReponse(String key, Object value) throws JSONException {

		respObject.put(key, value);
	}

	public void setErrorCode(String errorCode) throws JSONException {

		hogarResponse.put(AppConstants.CODE, errorCode);
	}

	public void setErrorMessage(String message) throws JSONException {

		hogarResponse.put(AppConstants.MESSAGE, message);
	}

	public void setUniqueId(String uniqueID) throws JSONException {

		hogarResponse.put(AppConstants.UNIQUE_ID, uniqueID);
	}

	@Override
	public String toString() {

		try {

			if (!hogarResponse.has(AppConstants.CODE))
				hogarResponse.put(AppConstants.CODE, AppErrorCode.SUCCESS);

			if (!hogarResponse.has(AppConstants.MESSAGE))
				hogarResponse.put(AppConstants.MESSAGE, AppErrorMessage.SUCCESS);

			hogarResponse.put(AppConstants.RESPONSE_CONTENT, respObject);
		} catch (JSONException e) {

		}

		return hogarResponse.toString();
	}

}
